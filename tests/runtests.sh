#!/bin/bash

GCMC="../src/gcmc"
OPTS="-I ../library --degrees"

TESTOUT="testoutput.ngc"

TESTFILES=( \
	"arccw.test.gcmc" \
	"arcs.test.gcmc" \
	"break.test.gcmc" \
	"circles.test.gcmc" \
	"compare.test.gcmc" \
	"const1.test.gcmc" \
	"const2.test.gcmc" \
	"const3.test.gcmc" \
	"const4.test.gcmc" \
	"const5.test.gcmc" \
	"continue.test.gcmc" \
	"defargs1.test.gcmc" \
	"defargs2.test.gcmc" \
	"defargs3.test.gcmc" \
	"floatint.test.gcmc" \
	"functions.test.gcmc" \
	"gcctl.test.gcmc" \
	"headtail.test.gcmc" \
	"if.test.gcmc" \
	"include.test.gcmc" \
	"index.test.gcmc" \
	"math.test.gcmc" \
	"movement.test.gcmc" \
	"reference.test.gcmc" \
	"scale.test.gcmc" \
	"star.test.gcmc" \
	"strings.test.gcmc" \
	"syntax.test.gcmc" \
	"units.test.gcmc" \
	"vcreate.test.gcmc" \
	"vector.test.gcmc" \
	)

TESTRETURNS=( \
	0 \
	0 \
	0 \
	0 \
	0 \
	0 \
	1 \
	1 \
	1 \
	1 \
	0 \
	1 \
	0 \
	1 \
	0 \
	1 \
	0 \
	0 \
	0 \
	0 \
	0 \
	0 \
	0 \
	0 \
	0 \
	0 \
	0 \
	1 \
	0 \
	0 \
	0 \
	)

ERRORS=0

$GCMC ${OPTS} -q -d "showversion.gcmc"
if [ $? -ne 0 ]; then
	echo "Error: Version number check failed"
	exit 1
fi

# Sanity check
if [ ${#TESTFILES[@]} -ne ${#TESTRETURNS[@]} ]; then
	echo "Error: Number of testfiles not equal to number of return values. Tests aborted."
	exit 1
fi

# Test all files for results
for((i=0; i<${#TESTFILES[@]}; i++))
do
	TESTFILE="${TESTFILES[$i]}"
	TESTEXPECT="$(basename ${TESTFILE} ".gcmc").result"
	TESTRETURN=${TESTRETURNS[$i]}
	if [ ! -r "${TESTFILE}" ]; then
		echo "Error: '${TESTFILE}' is missing"
		ERRORS=$(($ERRORS + 1))
		continue;
	fi
	if [ ! -r "${TESTEXPECT}" ]; then
		echo "Error: '${TESTFILE}' has missing result file '${TESTEXPECT}'"
		ERRORS=$(($ERRORS + 1))
		continue;
	fi
	$GCMC ${OPTS} -q -d "${TESTFILE}" > "${TESTOUT}" 2>&1
	RV=$?
	if [ $RV -ne $TESTRETURN ]; then
		echo "Error: '${TESTFILE}' compiled with unexpected return value $RV"
		ERRORS=$(($ERRORS + 1))
		continue;
	fi
	if ! diff -q "${TESTEXPECT}" "${TESTOUT}"; then
		echo "Error: '${TESTFILE}' has different result than expected. Differences:"
		diff -u "${TESTEXPECT}" "${TESTOUT}"
		mv -f "${TESTOUT}" "${TESTEXPECT}.error"
	fi
done

rm -f "${TESTOUT}"
exit $ERRORS
