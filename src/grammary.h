/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_GRAMMARY_H_INCLUDED
# define YY_YY_GRAMMARY_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    FUNCTION = 258,
    FOR = 259,
    FOREACH = 260,
    DO = 261,
    WHILE = 262,
    IF = 263,
    ELIF = 264,
    ELSE = 265,
    BREAK = 266,
    CONTINUE = 267,
    RETURN = 268,
    INCLUDE = 269,
    LOCAL = 270,
    REPEAT = 271,
    CONST = 272,
    TOPEN = 273,
    TCLOSE = 274,
    MM = 275,
    MIL = 276,
    IN = 277,
    DEG = 278,
    RAD = 279,
    IDENT = 280,
    STRING = 281,
    NUMBER = 282,
    FLOAT = 283,
    SUBASSIGN = 284,
    ADDASSIGN = 285,
    MULASSIGN = 286,
    DIVASSIGN = 287,
    MODASSIGN = 288,
    SHLASSIGN = 289,
    SHRASSIGN = 290,
    ADDORASSIGN = 291,
    SUBORASSIGN = 292,
    BORASSIGN = 293,
    BANDASSIGN = 294,
    BXORASSIGN = 295,
    LOR = 296,
    LAND = 297,
    EQ = 298,
    NE = 299,
    LT = 300,
    GT = 301,
    LE = 302,
    GE = 303,
    SHL = 304,
    SHR = 305,
    ADDOR = 306,
    SUBOR = 307,
    INC = 308,
    DEC = 309,
    UPM = 310,
    UID = 311
  };
#endif
/* Tokens.  */
#define FUNCTION 258
#define FOR 259
#define FOREACH 260
#define DO 261
#define WHILE 262
#define IF 263
#define ELIF 264
#define ELSE 265
#define BREAK 266
#define CONTINUE 267
#define RETURN 268
#define INCLUDE 269
#define LOCAL 270
#define REPEAT 271
#define CONST 272
#define TOPEN 273
#define TCLOSE 274
#define MM 275
#define MIL 276
#define IN 277
#define DEG 278
#define RAD 279
#define IDENT 280
#define STRING 281
#define NUMBER 282
#define FLOAT 283
#define SUBASSIGN 284
#define ADDASSIGN 285
#define MULASSIGN 286
#define DIVASSIGN 287
#define MODASSIGN 288
#define SHLASSIGN 289
#define SHRASSIGN 290
#define ADDORASSIGN 291
#define SUBORASSIGN 292
#define BORASSIGN 293
#define BANDASSIGN 294
#define BXORASSIGN 295
#define LOR 296
#define LAND 297
#define EQ 298
#define NE 299
#define LT 300
#define GT 301
#define LE 302
#define GE 303
#define SHL 304
#define SHR 305
#define ADDOR 306
#define SUBOR 307
#define INC 308
#define DEC 309
#define UPM 310
#define UID 311

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 74 "grammary.y" /* yacc.c:1909  */

	wchar_t		*str;
	double		d;
	int		i;
	node_t		*node;

#line 173 "grammary.h" /* yacc.c:1909  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_GRAMMARY_H_INCLUDED  */
